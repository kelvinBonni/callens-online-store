<!DOCTYPE html>
<html>
    <head>
        <title>Callens Online Store</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
<!--         <div class="container">
            <div class="content">
                <div class="title">Laravel 5</div>
            </div>
        </div> -->

        <a href="~/callens-online-store/resources/views/index.php">Home</a>
        <br/><br/>

        <!-- <form action="~/callens-online-store/resources/views/add.php" method="post" name="form1"> -->
        <form action="~/callens-online-store/resources/views/add.php" method="post" name="form1">
        <table width="25%" border="0">
            <tr> 
                <td>Name of Product</td>
                <td><input type="text" name="name"></td>
            </tr>
            <tr> 
                <td>Category</td>
                <td>
                <select>
                  <option value="Electronics">Electronics</option>
                  <option value="Home Appliances">Home Appliances</option>
                  <option value="Office Appliances">Office Appliances</option>
                </select>
                </td>  
            </tr>
            <tr> 
                <td>Amount</td>
                <td><input type="text" name="amount"></td>
            </tr>
            <tr> 
                <td></td>
                <td><input type="submit" name="Submit" value="Add"></td>
            </tr>
        </table>
    </form>
    </body>
</html>
